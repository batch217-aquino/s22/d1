//console.log("Hello Wordl!");

//[SECTION] ARRAY METHODS
//JS has built-in function and methods for array. This allows us to manipulate data within an array

//Mutator Methods
/*
    - Mutator methods are functions that "mutate" or change an array after they're created
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon fruits"];

//push()
/*
    - Adds an element in the end of an array AND returns the array's length
    - Syntax
        arrayName.push();
*/

console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

//Adding multiple elements/value to an array
fruits.push("avocado", "guava");
console.log("Mutated array from push method:");
console.log(fruits);

//pop()
/*
    - Removes the last element in an array AND returns the removed element
    - Syntax
        arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:")
console.log(fruits);
//fruits.pop();
//console.log("Mutated array from pop method:")
//console.log(fruits);

//unshift()
/*
    - Adds one or more elements at the beginning of an array
    - Syntax
        arrayName.unshift('elementA');
        arrayName.unshift('elementA', elementB);
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:")
console.log(fruits);

//shift()
/*
    - Removes an element at the beginning of an array AND returns the removed element
    - Syntax
        arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

//splice()
/* 
    - Simultaneously removes elements from a specified index number and adds elements
    - Syntax
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

//sort()
/*
    - Rearranges the array elements in alphanumeric order
    - Syntax
        arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

//reverse()
/*
    - Reverses the order of array elements
    - Syntax
        arrayName.reverse();
*/

fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

//Non-Mutator Methods
/*
    - Non-Mutator methods are functions that do not modify or change an array after they're created
    - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

//indexOf()
//Syntax --> arrayName.indexOf(SearchValue);
//Syntax --> arrayName.indexOf(SearchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry);

//lastIndexOf()
//Syntax --> arrayName.lastIndexOf(searchValue)

//getting the index number starting from the last element
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method " + lastIndex);

//getting the index number startng from a specified index
let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log("Result of lsatIndexOf method " + lastIndexStart);

//slice()
/*
- Portions/slices elements from an array AND returns a new array
- Syntax
    arrayName.slice(startingIndex);
    arrayName.slice(startingIndex, endingIndex);
*/

 // Slicing off elements from a specified index to the last element
 let slicedArrayA = countries.slice(2);
 console.log("Result from slice method:");
 console.log(slicedArrayA);

 // Slicing off elements from a specified index to another index
 let slicedArrayB = countries.slice(2, 4);
 console.log("Result from slice method:");
 console.log(slicedArrayB);

 // Slicing off elements starting from the last element of an array
 let slicedArrayC = countries.slice(-3);
 console.log("Result from slice method:");
 console.log(slicedArrayC);

 //toString()
/*
- Returns an array as a string separated by commas
- Syntax
    arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);

//concat()
/*
- Combines two arrays and returns the combined result
- Syntax
    arrayA.concat(arrayB);
    arrayA.concat(elementA);
*/

let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

//Combining multiple arrays
console.log("result from concat method:");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

//Combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method:");
console.log(combinedTasks);

//join()
/*
- Returns an array as a string separated by specified separator string
- Syntax
    arrayName.join('separatorString');
*/

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' ! '));

//Iteration Methods
//Iteration methods are loops designed to perform repetitive tasks on arrays
//Iteration methods loops over all items in an array.

//forEach()
//Similar to for loops
//arrayName.forEach(function(indivElement)){statement}

allTasks.forEach(function(tasks){
	console.log(tasks);
});

// Using forEach with conditional statements

let filteredTasks = [];

//Looping through all Array Items

allTasks.forEach(function(task){
	 // If the element/string's length is greater than 10 characters
	 if(task.length > 10){
	 	filteredTasks.push(task);
	 }
});

console.log("Result of filtered tasks:");
console.log(filteredTasks);

//map()
//Syntax let/const resultArray = arrayName.map(function(indivElement))

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});
console.log("Original Array:");
console.log(numbers); //original is unnafected by map()
console.log("Result of map method:");
console.log(numberMap); //A new array is return by map()

//map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number * number;
});

console.log(numberForEach); //undefined result

//every()
//- Checks if all elements in an array meet the given condition
//- This is useful for validating data stored in arrays especially when dealing with large amounts of data
//- Returns a true value if all elements meet the condition and false if otherwise

/*
	Syntax --> let/const resultArray = arrayName.every(function(){
		return expression/condition;
	})

*/

let allValid = numbers.every(function(number){
	return (number > 3);
});

console.log("Result of every method:");
console.log(allValid);

//some()

let someValid = numbers.some(function(number){
	return (number < 3);
});

console.log("Result from some method:");
console.log(someValid);

if(someValid){
	console.log("Some numbers in the array are greater than 3");
};

//filter()
/*

Syntax --> let/const resultArray = arrayName.filter(function(indivElement){
	return expression/condition
})

*/

let filterValid = numbers.filter(function(number){
	return (number < 4);
});

console.log("Result from filter method:");
console.log(filterValid);

//No elemet found
let nothingFound = numbers.filter(function(number){
	return (number = 0);
});

console.log("Result from filter method:");
console.log(nothingFound); //it will return empty brackets

//filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
});

console.log("Result from filter method:");
console.log(filteredNumbers);

//includes()
//Syntax --> arrayName.includes(<argument>)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

//reduce()
/*

Syntax let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
	return expression/condition;
})

*/

let iteration = 0;

let reduceArray = numbers.reduce(function(x, y){
	console.warn("current iteration: " + ++iteration)
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;
});

console.log("Result of reduce method: " + reduceArray);